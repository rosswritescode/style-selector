import React, { Component } from 'react';

import ToolBar from './components/ToolBar';

import './App.css';


/********************************************************
  Button set up object. Define name and value of buttons. 
********************************************************/
const buttonSetup = [{
  name: "Bold",
  value: "bold"
},{
  name: "Italic",
  value: "italic"
},{
  name: "Underlined",
  value: "underlined"
}];

/********************************************************
  App Holder
********************************************************/
class App extends Component {
  /********************************************************
    Constructor
  ********************************************************/
  constructor(props) {
    super(props);
    this.state = {
      toolsActive: ''
    }
  }

  /********************************************************
    Component events
  ********************************************************/
  toolsChanged = (toolstring) => {
    this.setState({
      toolsActive: toolstring
    });
  }

  // Demo method to change the toolbar value to a string,
  // which will be reflected in the toolbar button states.
  changeActive = () => {
    this.setState({
      toolsActive: 'bold,underlined'
    });
  }

  /********************************************************
    Render method
  ********************************************************/
  render() {
    // Add classes to the text div to show choices.
    let textClasses = this.state.toolsActive.split(',').map(tool => tool ? `text--${tool}` : '');
    
    return (
      <div className="App">

        <ToolBar
          buttonSetup={buttonSetup}
          onChange={this.toolsChanged}
          buttonsActive={this.state.toolsActive}
          multiselect={true} />
        
        <div className={'text ' + textClasses.join(' ')}>
          Text editor: { this.state.toolsActive }
        </div>

        <button onClick={this.changeActive}>Set to bold and underlined</button>

      </div>
    );
  }
}

export default App;
