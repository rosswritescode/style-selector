import React, { Component } from 'react';

import ToggleButton from './ToggleButton';

import './ToolBar.css';

/********************************************************
  ToolBar:
    > Creats a toolbar of buttons as defined by the
      this.props.buttonSetup object.
    > Reports status via onChange prop, sends back a 
      string with comma seperated values.
********************************************************/
class ToolBar extends Component {

  /********************************************************
    Constructor
  ********************************************************/
  constructor(props) {
    super(props);
    this.state = {
      valuesObject: {},
      value: this.props.buttonsActive || ''
    }
  }

  /********************************************************
    React events
  ********************************************************/
  componentWillMount() {
    // Set default buttons on mounting
    this.parseValueString(this.props.buttonsActive);
  }

  componentDidUpdate = (prevProps, prevState) => {
    // Create values string if the values object is updated
    if(prevState.valuesObject !== this.state.valuesObject) {
      let valuesObject = this.state.valuesObject;
      let value = Object.keys(valuesObject).filter(key => valuesObject[key]).join(',');

      // Set value string in state.
      this.setState({
        value
      });
    }

    // If we've a new value string, send it to the onChange event.
    if(prevState.value !== this.state.value) {
      this.props.onChange(this.state.value);
    }
  }
  
  shouldComponentUpdate = (nextProps, nextState) => {    
    // If we've a new active button string, toggle the right options.
    if(nextProps.buttonsActive !== this.props.buttonsActive) {
      this.parseValueString(nextProps.buttonsActive);
    }

    return true;
  }

  /********************************************************
    Class functions
  ********************************************************/
  parseValueString(valueString) {
    // Parse a comma seperated string into an object to
    // be used by buttonclicked method.
    const valuesArray = valueString.split(',');
    const valuesObject = {};
    
    valuesArray.map(value => value ? valuesObject[value] = true : false);

    this.setState({
      valuesObject
    });
  }

  /********************************************************
    Component events
  ********************************************************/
  buttonClicked = (value, on) => {
    this.setState({
      valuesObject: this.props.multiselect ? { ...this.state.valuesObject, [value]: on } : { [value]: on }
    });
  }

  /********************************************************
    Render methods
  ********************************************************/
  renderButtons() {
    const buttons = [];
    
    // eslint-disable-next-line
    this.props.buttonSetup.map(option => {
      buttons.push(<ToggleButton
          key={option.name}
          name={option.name}
          value={option.value}
          onToggle={this.buttonClicked}
          isPressed={this.state.value.indexOf(option.value) !== -1}/>)
    });

    return buttons;
  }

  render() {
    return (
      <div className="toolbar">
        { this.renderButtons() }
      </div>
    );
  }
}

export default ToolBar;
