import React, { Component } from 'react';

import './ToggleButton.css';

/********************************************************
  ToggleButton:
    > A button that can be toggled on or off.
********************************************************/
class ToggleButton extends Component {
  
  /********************************************************
    Component events
  ********************************************************/
  toggle = (event) => {
    this.props.onToggle(this.props.value, !this.props.isPressed);
  }

  /********************************************************
    Render method
  ********************************************************/
  render() {
    return (
      <button
        className="toggle-button"
        value={this.props.value}
        onClick={this.toggle}
        aria-pressed={this.props.isPressed}>
          {this.props.name}
      </button>
    );
  }
}

export default ToggleButton;
