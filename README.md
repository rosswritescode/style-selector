This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

# Introduction
This is a simple toolbar component, where you can define any number of buttons and have them appear like a toolbar. 

The toolbar component exposes a comma seperated string of the values that are toggled.

The parent component of the toolbar defines what buttons are created, and what happens when the value of the component changes.

# Running the project

1. Clone repo, go to folder, run ```npm install```

2. Run ```npm start```. This will  run the React app.

3. If it does not open automatically, go to `http://localhost:3000/`

# Using the toggle buttons

Once you have the app opened in a browser, start toggling buttons. You'll see the text change.

You can also click the "Set to bold and underlined" button, which gives the toolbar component a "bold,underlined" string which it then uses to press the correct buttons.
 log in


# Contact

mail@rosswritescode.com